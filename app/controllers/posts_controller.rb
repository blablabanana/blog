class PostsController < ApplicationController
	before_action :signed_in_user
	before_action :correct_user,   only: :destroy

	def create
		@post = current_user.posts.build(post_params)
		if @post.save
			redirect_to root_url
		end
	end

	def destroy
		@post.destroy
		respond_to do |format|
			format.html { redirect_to current_user }
			format.js
		end
	end

	def edit
    @post = Post.find(params[:id])
  end

  def update
  	@post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      redirect_to current_user
    else
      render 'edit'
    end
  end

	private
		def post_params
			params.require(:post).permit(:content)
		end

		def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end

end
