class CommentsController < ApplicationController
	before_action :signed_in_user
	before_action :correct_user, only: :destroy
	before_action :set_commentable, only: :create


	def new
	  @comment = Comment.new(params[:comment])
	end

	def create
		@comment = @commentable.comments.new comment_params
		@comment.user = current_user
		if @comment.save
			redirect_to root_url, notice: 'Your comment was created'
		else
			render current_user
		end	 
	end

	def destroy
		@comment.destroy
		respond_to do |format|
			format.html { redirect_to current_user }
			format.js
		end
	end

	def edit
		# @comment = @commentable.comments.find(params[:id])
    @comment = Comment.find(params[:id])
  end

  def update
  	@comment = Comment.find(params[:id])
    if @comment.update_attributes(comment_params)
      redirect_to current_user
    else
      render 'edit'
    end
  end

	private

		def comment_params
			params.require(:comment).permit(:body)
		end

		def correct_user
      @comment = current_user.comments.find(params[:id])
      redirect_to root_url if @comment.nil?
    end

end
