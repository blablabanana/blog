# [Blog](https://salty-ridge-18225.herokuapp.com/)

Based on Michael Hartl's [Railstutorial](https://www.railstutorial.org/). Developed as training app.

## Used gems
- bcrypt
- paperclip

##App allows to:
- Sign up
- Edit profile (name, email, password, avatar)
- Follow other users
- Write, delete, edit comments and posts
- Bad redirects

## Whats wrong with this blog:
- missing flash messages
- a lot of duplicade code
- bad views structure
-  a lot of commits are in the main branch (because at first branches was created and merg ed localy)
- strange redirects
- avatars crash after pushing app on Heroku(fix by adding amazon s3 service fot image storing)