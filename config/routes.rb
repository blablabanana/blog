Rails.application.routes.draw do

	resources :users do
		member do
			get :following, :followers
		end
	end	
	resources :sessions, 	 only: [:new, :create, :destroy]
	resources :posts do
		resources :comments, module: :posts 
	end
	resources :comments do
		resources :comments, module: :comments
	end
	resources :relationships, only: [:create, :destroy]

	root 'pages#index'
	match '/signup',  to: 'users#new', 	  		via: 'get'
	match '/signin',  to: 'sessions#new', 		via: 'get'
	match '/signout', to: 'sessions#destroy',	via: 'delete'
end
